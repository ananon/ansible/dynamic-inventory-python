# About
This doc explains how to us the inventory script in this repo, how to add it to the ansible tower.

## Special things to notice when working on an inventory script
### The inventory script has to be ***executable***
For that we have to do two things: 
1. In order to maintain the script's permission to be executable through git, we have to use this command before pushing it for the first time:

```bash
git update-index --chmod=+x <file_name>
```
So now when the inventory in the Ansible Tower updates or is being used, Ansible will be able to execute the file.

2. The script file has to start with Shebang and the file for the correct interpreter, like so:
```bash
#!/usr/bin/python
```

### It doesn't matter how you write your script
As long as the Ansible server can execute it, you are free to use any language you wish. you can even build a binary if you want.

### Don't edit in Pycharm on Windows
This is a long shot and I haven't checked it 100% but, for some reason you can read about [here](https://unix.stackexchange.com/questions/32001/what-is-m-and-how-do-i-get-rid-of-it) (although the solution offered didn't help me), the pair '^M' ended every line.
Like \n or \r we are used to. It couldn't be seen when accessing the file on vim or printing it, but I know that was the case because this error message after trying to test locally on the server:
```
-bash: ./inventory-python-2: /usr/bin/python^M: bad interpreter: No such file or directory
```
That ^M shouldn't be there, and after creating that same file again in vim, it worked.
I don't really know what causes that and if any file created on windows will generate the same error when executed, I will continue to investigate on that later.

## In Ansible Tower

1. Create a project in Tower and point it to the git repo where the inventory script sits.
2. Create or edit the inventory you want, in the inventory go to SOURCES -> "+" button to create new source -> under "choose source" select Sourced from a project -> select the project pointing at your repo - > under "INVENTORY FILE" choose the folder in which the inventory script sits (it should automatically suggest the options it the project is set up correctly, in my case it was just '/' for root)
3. Update the project and sync the inventory script to see that it all runs good.

## The script
The script I took is an example script that should be tested, it is based on [this link](https://www.jeffgeerling.com/blog/creating-custom-dynamic-inventories-ansible).
You can also view the guidelines for an inventory provider script like the one we are trying to set [here](https://docs.ansible.com/ansible/2.5/dev_guide/developing_inventory.html).
### Groups hosts and variables
It is easy to find examples that define groups, hosts and their variables, but I had a harder time finding the documentation for defining the 'global' vars like we use in our normal inventory today. eventually I found two ways of adding them:
1. Creating a directory called *group_var* and a file called *all.yml*, then just add your vars there like a normal yml.
2. Add a group in the returned json called all, it will include all of the other groups as children plus the 'ungrouped'; and a vars block where we can add our global variables. like so:

```json
'all':{
    'children':[
        'group',
        'ungrouped'
    ],
    'vars':{
        'user1': 'admin',
        'user2': 'root'
    }
},
```

Both of the methods can work together.
